const path = require('path');

module.exports = {
    entry: './auth.js',
    output: {
        filename: 'main-module.js',
        path: path.resolve(__dirname, 'dist'),
        library: {
            type: "module"
        }
    },
    mode: 'production',
    experiments: {
        outputModule: true
    }
};
