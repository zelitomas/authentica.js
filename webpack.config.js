const path = require('path');

module.exports = {
    entry: './auth.js',
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, 'dist'),
        library: {
            name: "Authenticator",
            type: "var",
            export: 'default',

        }
    },
};
